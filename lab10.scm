;add 
(define add 
	(lambda (m n s z)
              (m s(n s z))))
;subtract
(define subtract 
	(lambda (m n)
              (n pred m)))
;not
(define NOT
	(lambda (M)
              (lambda(a b)
		(M b a))))
;and
(define AND
	(lambda (M N)
              (lambda(a b)
		(N (M a b) b))))
;or
(define OR
	(lambda (M N)
              (lambda(a b)
		(N a (M a b)))))
;LEQ
(define LEQ 
	(lambda (m n)
              (IsZero(n pred m))))
;GEQ
(define GEQ
	(lambda (a b)
		(NOT(AND( (NOT(EQ a b)) (LEQ a b) )))))
